<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'emailtospip_description' => 'Permet de publier vos articles par email',
	'emailtospip_nom' => 'Publication par email',
	'emailtospip_slogan' => 'Publier vos articles via email',
);

?>